Rails.application.routes.draw do
  root                'home#index'
  get 'about'     =>  'home#about'
  get 'contact'   =>  'home#contact'
  get 'faq'       =>  'home#faq'
  get 'policy'    =>  'home#policy'
#  post 'contact'  =>  'home#contact'

  devise_for :users, controllers: {
    confirmations: "users/confirmations",
    mailer: "users/mailer",
    passwords: "users/passwords",
    registrations: "users/registrations",
    sessions: "users/sessions",
    shared: "users/shared",
    unlocks: "users/unlocks"
  }
  resources :users do
    resources :groups
  end
  resources :groups do
    resources :users
  end
  resources :contributions
  resources :vote_challenges
  resources :vote_comments
  resources :comments
  resources :challenges
  resources :categories do
    resources :challenges
  end

  resources :charges
end

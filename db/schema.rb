# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151031223024) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accepts", force: :cascade do |t|
    t.integer  "acceptor_id"
    t.string   "acceptor_type"
    t.integer  "accepted_id"
    t.string   "accepted_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "accepts", ["accepted_type", "accepted_id"], name: "index_accepts_on_accepted_type_and_accepted_id", using: :btree
  add_index "accepts", ["acceptor_type", "acceptor_id"], name: "index_accepts_on_acceptor_type_and_acceptor_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories_challenges", id: false, force: :cascade do |t|
    t.integer "challenge_id"
    t.integer "category_id"
  end

  add_index "categories_challenges", ["challenge_id", "category_id"], name: "index_categories_challenges_on_challenge_id_and_category_id", using: :btree

  create_table "challenges", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "moderation_flag"
    t.string   "status"
    t.text     "terms"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "challenge_id",                   null: false
    t.text     "requirement"
    t.date     "deadline"
    t.integer  "payoff"
    t.boolean  "open_flag",       default: true
    t.integer  "min_num"
    t.integer  "max_num"
  end

  create_table "challenges_users", id: false, force: :cascade do |t|
    t.integer "challenge_id"
    t.integer "user_id"
  end

  add_index "challenges_users", ["challenge_id", "user_id"], name: "index_challenges_users_on_challenge_id_and_user_id", using: :btree

  create_table "charges", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "challenge_id"
    t.text     "description"
    t.decimal  "amount",       precision: 8, scale: 2
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "charges", ["user_id"], name: "index_charges_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "challenge_id"
    t.text     "comment"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "contacts", ["user_id"], name: "index_contacts_on_user_id", using: :btree

  create_table "contributions", force: :cascade do |t|
    t.integer  "contributor_id"
    t.string   "contributor_type"
    t.integer  "contributed_id"
    t.string   "contributed_type"
    t.string   "charity"
    t.integer  "amount"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "contributions", ["contributed_type", "contributed_id"], name: "index_contributions_on_contributed_type_and_contributed_id", using: :btree
  add_index "contributions", ["contributor_type", "contributor_id"], name: "index_contributions_on_contributor_type_and_contributor_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "groups_users", id: false, force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
  end

  add_index "groups_users", ["group_id", "user_id"], name: "index_groups_users_on_group_id_and_user_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "payment_id",                            default: "nextval('payment_id_seq'::regclass)", null: false
    t.string   "username"
    t.integer  "challenge_id"
    t.integer  "last_4_digits"
    t.string   "card_type"
    t.string   "firstname"
    t.string   "lastname"
    t.datetime "date"
    t.string   "status"
    t.datetime "created_at",                                                                            null: false
    t.datetime "updated_at",                                                                            null: false
    t.decimal  "amount",        precision: 8, scale: 2
  end

  create_table "transactions", force: :cascade do |t|
    t.string   "transaction_type"
    t.integer  "challenge_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "transactions_users", id: false, force: :cascade do |t|
    t.integer "transaction_id"
    t.integer "user_id"
  end

  add_index "transactions_users", ["transaction_id", "user_id"], name: "index_transactions_users_on_transaction_id_and_user_id", using: :btree

  create_table "user_groups", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_groups", ["group_id"], name: "index_user_groups_on_group_id", using: :btree
  add_index "user_groups", ["user_id"], name: "index_user_groups_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.date     "date_of_birth"
    t.string   "phone_number"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vote_challenges", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "challenge_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "vote_comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "comment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "charges", "users"
  add_foreign_key "contacts", "users"
  add_foreign_key "user_groups", "groups"
  add_foreign_key "user_groups", "users"
end

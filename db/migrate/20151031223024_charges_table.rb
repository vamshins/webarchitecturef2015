class ChargesTable < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.references :user, index: true, foreign_key: true
      t.references :challenge
      t.text :description, :limit => 100
      t.decimal  :amount,        precision: 8, scale: 2
      t.timestamps null: false
    end
  end
end
